.PHONY: all clean install

INST_PREFIX  ?= /usr/local
INST_BINDIR  ?= $(INST_PREFIX)/bin
INST_LIBDIR  ?= $(INST_PREFIX)/lib/lua/5.1
INST_LUADIR  ?= $(INST_PREFIX)/share/lua/5.1
INST_CONFDIR ?= $(INST_PREFIX)/etc

SRC = $(shell find . -name "*.moon")
OUT = $(SRC:.moon=.lua)


all: $(OUT)

%.lua: %.moon
	@moonc $^

clean:
	$(RM) $(OUT)

install:
	@find plexiclass -name "*.lua" -and -not -name "package.lua" | xargs cp --parents -t $(INST_LUADIR)
