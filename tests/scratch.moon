import Object from require "plexiclass.Object"
import Trait from require "plexiclass.Trait"
import p from require "moon"


-- Biped = Trait "tests.Biped", =>

--     obj overridable: =>
--         print parent
--         parent @
--         print "Biped"


-- Person = Object\extend "tests.Person", =>

--     obj overridable: =>
--         print parent
--         print "Person"

--     use Biped

--     cls pro max:
--         type: "number"


--     cls defer name: =>
--         print "get_name accessed"
--         "undefined"

--     obj pro name:
--         type: "string"

--     obj say_hello: =>
--         print "hello"

--     obj get useless_fn: =>
--     obj del -> get useless_fn

--     obj __tostring: =>
--         @@name .. ": " .. @name


-- risitas = Person name: "Risitas"
-- risitas\overridable!

-- print risitas
-- print risitas\clone!


-- class Biped
--     new: =>
--         print "prout"

--     aya: => "xd"

-- class Person extends Biped
--     @gneugneu: => "prout"
--     aya: => "heughenue " .. super\aya!

-- Person = Object\extend_with_moonscript_class Person
-- person = Person!
-- print person\aya!


import List from require "plexiclass.List"

l1 = List 15, 16, 14, 25
print l1\join ", "
print l1

l2 = List!
print l2\join ", "
print l2

print l1\unpack!

cjson = require "cjson"
print cjson.encode l1\to_data!
print cjson.encode l2\to_data!
