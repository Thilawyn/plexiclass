import Object, Set, List from require "plexiclass"
import p                 from require "moon"


ImageModel = Object\extend "tests.ImageModel", =>

    obj pro id:
        type: "number"
    obj pro src:
        type: "string"

    obj __tostring: =>
        @@name .. "(" .. @id .. ", " .. @src .. ")"


data = {
    images: {
        { id: 1, src: "1.png" }
        { id: 2, src: "2.jpg" }
        { id: 3, src: "3.webp" }
    }
}

print ImageModel\to_data!
p data

parsed_data = Set\parse data,
    keys:
        images:
            class: List

            all:
                class: ImageModel

print parsed_data

p parsed_data\to_data!
