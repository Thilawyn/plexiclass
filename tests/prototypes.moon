import p from require "moon"
import Prototype from require "plexiclass.prototypes.Prototype"
import ClassPrototype from require "plexiclass.prototypes.ClassPrototype"


p1 = with Prototype!
    \set_property "name",
        type: "string"
    \set_property "x",
        type: "number"
        default: 0
    \set_property "y",
        type: "number"
        default: 0

p2 = with Prototype p1
    \set_property "name",
        default_deffered: => "aya"

p3 = ClassPrototype!

p p3
