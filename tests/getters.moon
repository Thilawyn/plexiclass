import Object from require "plexiclass"


MyObject = Object\extend "tests.MyObject", =>

    obj defer deferred_value: =>
        print "deferred_value getter accessed"
        "a value"


obj = MyObject!
print obj.deferred_value
print obj.deferred_value

print obj[3]
