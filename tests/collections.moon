import List   from require "plexiclass.List"
import Set    from require "plexiclass.Set"
import Object from require "plexiclass.Object"


l = List 1, 2, 8, 65, 21
-- print l

-- l = List\from_data {1, 2, 8, 65, 21}
print l
print l\map (k, v) -> v * 2
print l\accept (k, v) -> v % 2 == 0

-- print l + List 5, 18, 36

-- l = List {1, 2, 8, 65, 21}
-- print l\sort!
-- print l

-- l\remove_first!
-- l\remove_last!
-- print l
-- print l.length

-- print l\reduce ((str, k, v) -> str .. " " .. v), ""

-- Dated = Object\extend "Dated", =>
--     obj pro date:
--         type: "number"

--     obj __eq: (d) => @date == d.date
--     obj __lt: (d) => @date <  d.date
--     obj __le: (d) => @date <= d.date

--     obj __tostring: =>
--         "Date: " .. @date

-- l2 = List {
--     Dated date: 12
--     Dated date: 1
--     Dated date: 3
-- }
-- print l2
-- l2\sort!
-- print l2

-- print l2\first_where_key "date", 12

-- my_set = Set
--     aya: "issou"
--     :l
-- print my_set
-- print my_set\reject (k, v) ->
--     v == "issou"
