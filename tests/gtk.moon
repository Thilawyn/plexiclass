lgi = require "lgi"
Gtk = lgi.require "Gtk"

import LGIObject from require "plexiclass.LGIObject"

-- import p from require "moon"


MyGtkWindow = LGIObject\extend "tests.MyGtkWindow", (parent_cls, parent_obj) =>
    cls __lgi_class: Gtk.Window

my_win = MyGtkWindow!
print type my_win.property
-- my_win\show_all!
