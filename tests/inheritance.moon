import Object from require "plexiclass.Object"
import p      from require "moon"

Class1 = Object\extend "Class1", =>
    obj pro pro1:
        type: "string"
    obj pro pro2:
        type: "string"

p Class1.__instance_prototype.__properties_by_order


Class2 = Class1\extend "Class2", =>
    obj pro pro1:
        type: "number"

Class3 = Class2\extend "Class3", =>


p Class3.__instance_prototype.__properties_by_order
