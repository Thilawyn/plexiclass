export gneugneu

import Object  from require "plexiclass.Object"
import setfenv from require "plexiclass.util"


Parent = Object\extend "Parent", =>
    obj extendable_method: =>
        "issou"

Class1 = Parent\extend "Class1", =>
    obj method: =>
        gneugneu

    obj extendable_method: =>
        "aya " .. parent.extendable_method @

setfenv Class1.__prototype.__instance_prototype.method, setmetatable { gneugneu: "aya" }, __index: _G

obj1 = Class1!


issou = "aya"
extendable_method = Parent.__instance_prototype.extendable_method
Class2 = Parent\extend "Class2", =>
    obj method: =>
        issou

    obj extendable_method: =>
        "aya " .. extendable_method @

obj2 = Class2!


TEST_COUNT = 1000000

s = os.clock!
for i = 1, TEST_COUNT
    obj1\extendable_method!
t1 = os.clock! - s

s = os.clock!
for i = 1, TEST_COUNT
    obj2\extendable_method!
t2 = os.clock! - s


slowest = t1 > t2 and t1 or t2
fastest = t1 < t2 and t1 or t2
diff    = (slowest - fastest) / fastest * 100

print "Global access:   #{ t1 } #{ t1 == fastest and "<==" or "" }"
print "Local access:    #{ t2 } #{ t2 == fastest and "<==" or "" }"
print "Difference :     +#{ diff }%"
