-- rockspec_format = "3.0"

package = "plexiclass"
version = "dev-3"

source = {
    url    = "git+https://gitlab.com/Thilawyn/plexiclass.git",
    branch = "master",
}

description = {
    summary    = "Alternative OOP implementation for MoonScript",
    detailed   = "Alternative OOP implementation for MoonScript. Provides traits, object properties, metamethod inheritance, collections and many more cool new toys!",
    license    = "MIT",
    homepage   = "https://gitlab.com/Thilawyn/plexiclass",
    -- issues_url = "https://gitlab.com/Thilawyn/plexiclass/-/issues",
    maintainer = "Julien Valverdé <julien.valverde@mailo.com>",
    -- labels     = {"moonscript", "oop"},
}

dependencies = {
    "lua >= 5.1",
    "moonscript",
    "lua-cjson",
}

-- build_dependencies = {
--     "moonscript",
-- }

build = {
    type = "make",
    build_variables = {
        CFLAGS     = "$(CFLAGS)",
        LIBFLAG    = "$(LIBFLAG)",
        -- LUA_LIBDIR = "$(LUA_LIBDIR)",
        LUA_BINDIR = "$(LUA_BINDIR)",
        LUA_INCDIR = "$(LUA_INCDIR)",
        LUA        = "$(LUA)",
    },
    install_variables = {
        INST_PREFIX  = "$(PREFIX)",
        INST_BINDIR  = "$(BINDIR)",
        INST_LIBDIR  = "$(LIBDIR)",
        INST_LUADIR  = "$(LUADIR)",
        INST_CONFDIR = "$(CONFDIR)",
    },
}
