globals = {
    "Class"
    "Object"
    "Trait"
    "LGIObject"

    "List"
    "Map"

    "type"
    "is"

    "fn"
    "cls"
    "obj"
    "del"
    "get"
    "set"
    "defer"
    "pro"
    "use"

    "parent"
}


---
inject_whitelist_globals: (t) ->
    for global in *globals
        t[ #t + 1 ] = global

    t
