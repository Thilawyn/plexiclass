-- Luvit compatibility
if pcall require, "luvi"
    -- Preload the scripts with Luvit's loader
    with package.loaded
        ["plexiclass.util"]                          = require "./util"
        ["plexiclass.type"]                          = require "./type"

        ["plexiclass.methods.class"]                 = require "./methods/class"
        ["plexiclass.methods.instance"]              = require "./methods/instance"
        ["plexiclass.methods.metamethods"]           = require "./methods/metamethods"
        ["plexiclass.methods.properties"]            = require "./methods/properties"
        ["plexiclass.methods.set"]                   = require "./methods/set"
        ["plexiclass.methods.class_serialize"]       = require "./methods/class_serialize"
        ["plexiclass.methods.instance_serialize"]    = require "./methods/instance_serialize"

        ["plexiclass.prototypes.Prototype"]          = require "./prototypes/Prototype"
        ["plexiclass.prototypes.InstancePrototype"]  = require "./prototypes/InstancePrototype"
        ["plexiclass.prototypes.ClassPrototype"]     = require "./prototypes/ClassPrototype"

        ["plexiclass.methods.metaclass"]             = require "./methods/metaclass"

        ["plexiclass.prototypes.MetaclassPrototype"] = require "./prototypes/MetaclassPrototype"

        ["plexiclass.Class"]                         = require "./Class"
        ["plexiclass.Object"]                        = require "./Object"
        ["plexiclass.Trait"]                         = require "./Trait"
        ["plexiclass.LGIObject"]                     = require "./LGIObject"
        ["plexiclass.Collection"]                    = require "./Collection"
        ["plexiclass.List"]                          = require "./List"
        ["plexiclass.Set"]                           = require "./Set"


{
    Class:      require("plexiclass.Class").Class
    Object:     require("plexiclass.Object").Object
    Trait:      require("plexiclass.Trait").Trait

    LGIObject:  require("plexiclass.LGIObject").LGIObject

    List:       require("plexiclass.List").List
    Set:        require("plexiclass.Set").Set

    type:       require("plexiclass.type").type
    is:         require("plexiclass.type").is
}
