import Collection from require "plexiclass.Collection"

import is from require "plexiclass.type"

import
    getmetatable
    error
    pairs
from _G

unpack or= table.unpack


Set: Collection\extend "plexiclass.Set", =>

    obj get length: =>
        l = 0
        @each -> l += 1

        l


    obj each: (f) =>
        for k, v in pairs @__values
            f k, v

        @

    obj each_until: (f) =>
        for k, v in pairs @__values
            res = { f k, v }
            if #res > 0
                return unpack res

    obj append: (m) =>
        m\each (k, v) ->
            @[k] = v

        @

    obj append_table: (t) =>
        for k, v in pairs t
            @[k] = v

        @

    obj __tostring: =>
        str = "{\n"
        @each (k, v) ->
            str ..= "\t#{ k }: #{ v }\n"

        str .. "}"


    --
    -- Removing
    --

    obj remove: (k) =>
        @[ k ] = nil

        @


    --
    -- Filtering
    --

    obj accept: (f) =>
        s = @@!
        @each (k, v) ->
            if f k, v
                s[ k ] = v

        s

    obj reject: (f) =>
        s = @@!
        @each (k, v) ->
            if not f k, v
                s[ k ] = v

        s

    obj accept_self: (f) =>
        -- TODO

    obj reject_self: (f) =>
        -- TODO
