{
    name:          "Thilawyn/plexiclass"
    version:       "0.1.7"
    description:   "Alternative OOP implementation for MoonScript"
    tags:          {"moonscript", "oop"}
    license:       "MIT"
    author:
        name:   "Julien Valverdé"
        email:  "julien.valverde@mailo.com"
    homepage:      "https://gitlab.com/Thilawyn/plexiclass"
    dependencies:  {"lduboeuf/cjson"}
    files:         {"**.lua"}
}
