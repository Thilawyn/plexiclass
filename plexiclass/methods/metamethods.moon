import tostring, rawget from _G


{
    __index: (k) =>
        -- If this attribute has a value
        v = @__values[ k ]
        if v != nil
            return v

        -- If there is a getter for this attribute
        if getter = @__prototype[ "get_#{ k }" ]
            return getter @, k

        -- Else, search the prototype
        rawget @__prototype, k

    __newindex: (k, v) =>
        -- If there is a setter for this attribute
        if setter = @__prototype[ "set_#{ k }" ]
            setter @, k, v
            return

        @__values[ k ] = v
}
