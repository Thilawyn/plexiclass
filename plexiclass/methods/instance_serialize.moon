import is    from require "plexiclass.type"
import pairs from _G


{
    clone: =>
        values = {}
        for k, v in pairs @__values
            values[ k ] = v

        @@from_data values

    to_data: =>
        data = {}

        for k, v in pairs @__values
            data[ k ] = if is(v, "plexiclass.Class") or is(v, "plexiclass.Object")
                v\to_data!
            else
                v

        data
}
