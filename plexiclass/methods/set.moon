import
    getmetatable
    pairs
    rawset
    assert
    tostring
from _G

import is_metamethod, setfenv from require "plexiclass.util"
import remove                 from table
import is, type               from require "plexiclass.type"


set_function_metavalues = (f, name, prototype, parent_prototype) ->
    t = setmetatable {
        :name
        :prototype
    }, __index: _G

    if parent_prototype
        t.parent = parent_prototype[ name ]

    setfenv f, t

delete_values = (token) ->
    setfenv token.value, setmetatable {
        pro: (v) -> token.prototype\remove_property v
        get: (v) -> "get_" .. v
        set: (v) -> "set_" .. v
    }, __index: (_, k) ->
        k

    res = token.value!
    switch type res
        when "table"
            for key in *token.value!
                token.prototype[ key ] = nil

        when "string"
            token.prototype[ res ] = nil

--- Runs through the given tokens and applies them.
parse_tokens = (tokens) =>
    for token in *tokens
        switch token.type
            when "delete"
                delete_values token

            when "property"
                token.prototype\set_property token.key, token.value

            when "deferred"
                set_function_metavalues token.value, "get_" .. token.key, token.prototype, token.parent_prototype
                token.prototype\set_deferred_attribute token.key, token.value

            else
                if type(token.value) == "function"
                    set_function_metavalues token.value, token.key, token.prototype, token.parent_prototype

                token.prototype[ token.key ] = token.value

                -- If a metamethod is being added to the class prototype, add it to the class object too.
                if token.prototype == @__prototype and is_metamethod[ token.key ]
                    rawset @, token.key, token.value


set = (f, parent_prototype, ...) =>
    env =
        fn: (f) ->
            set_function_metavalues f

        cls: (values) ->
            tokens = {}

            for key, value in pairs values
                switch type key
                    when "number"  -- Token
                        value.prototype        = @__prototype
                        value.parent_prototype = parent_prototype or @__prototype.__parent_prototype

                        tokens[ #tokens + 1 ] = value

                    when "string"  -- Value
                        tokens[ #tokens + 1 ] =
                            :key
                            :value
                            prototype:         @__prototype
                            parent_prototype:  parent_prototype or @__prototype.__parent_prototype

            parse_tokens @, tokens

        obj: (values) ->
            tokens = {}

            for key, value in pairs values
                switch type key
                    when "number"  -- Token
                        value.prototype        = @__instance_prototype
                        value.parent_prototype = parent_prototype and parent_prototype.__instance_prototype or @__instance_prototype.__parent_prototype

                        tokens[ #tokens + 1 ] = value

                    when "string"  -- Value
                        tokens[ #tokens + 1 ] =
                            :key
                            :value
                            prototype:         @__instance_prototype
                            parent_prototype:  parent_prototype and parent_prototype.__instance_prototype or @__instance_prototype.__parent_prototype

            parse_tokens @, tokens

        del: (value) ->
            { {type: "delete", :value} }

        pro: (values) ->
            tokens = {}
            for key, value in pairs values
                if type(key) == "string"
                    tokens[ #tokens + 1 ] = type: "property", :key, :value

            tokens

        get: (values) ->
            tokens = {}
            for key, value in pairs values
                if type(key) == "string"
                    tokens[ #tokens + 1 ] = key: "get_" .. key, :value

            tokens

        set: (values) ->
            tokens = {}
            for key, value in pairs values
                if type(key) == "string"
                    tokens[ #tokens + 1 ] = key: "set_" .. key, :value

            tokens

        defer: (values) ->
            tokens = {}
            for key, value in pairs values
                if type(key) == "string"
                    tokens[ #tokens + 1 ] = type: "deferred", :key, :value

            tokens

        ---
        use: (trait, ...) ->
            assert is(trait, "plexiclass.Trait"), "bad argument #1 to 'use' (plexiclass.Trait expected, got #{ type trait })"  -- A trait must have been given
            assert not @has_trait(trait), "#{ trait } is already included in #{ @ }"                                           -- This trait must not have been already included

            @set trait.definition, @__prototype\clone_prototype!, ...
            @__prototype.__traits_by_order[ #@__prototype.__traits_by_order + 1 ] = trait

    setfenv f, setmetatable(env, __index: _G)

    f @, ...

    nil


:set
