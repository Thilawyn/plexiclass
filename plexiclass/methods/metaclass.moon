export _
import
    error
    assert
from _G

import is, type from require "plexiclass.type"

import ClassPrototype from require "plexiclass.prototypes.ClassPrototype"


{
    --
    -- Metamethods
    --

    __tostring: =>
        "metaclass"


    --
    -- Type
    --

    __name: "plexiclass.Class"

    type: =>
        "metaclass"

    is: (t) =>
        t == "metaclass"


    --
    -- Class creation
    --

    __call: (a1, a2, a3) =>
        if     a1 == nil and a2 == nil and a3 == nil            -- Class!
            @create!

        elseif is(a1, "string") and is(a2, "plexiclass.Class")  -- Class name, parent, (definition)
            @create a1, a2, a3

        elseif is(a1, "string")                                 -- Class name, (definition)
            @create a1, _, a2

        elseif is(a1, "plexiclass.Class")                       -- Class parent, (definition)
            @create _, a1, a2

        elseif is(a1, "function")                               -- Class definition
            @create _, _, a1

        else
            error "bad arguments to '#{ @__name }'. Valid calls:\n\t- #{ @__name }()\n\t- #{ @__name }(string, plexiclass.Class, function)\n\t- #{ @__name }(string, function)\n\t- #{ @__name }(plexiclass.Class, function)\n\t- #{ @__name }(function)"

    --- Creates a class.
    create: (name, parent, definition = ->) =>
        assert is(name, "string"),             "bad argument #1 to 'create' (function expected, got #{ type name })"  if name
        assert is(parent, "plexiclass.Class"), "bad argument #2 to 'create' (plexiclass.Class expected, got #{ type parent })"  if parent
        assert is(definition, "function"),     "bad argument #3 to 'create' (function expected, got #{ type definition })"

        with cls = ClassPrototype(name, parent)\create_object!
            .__prototype.__class          = @
            .__instance_prototype.__class = cls

            \set definition
            \initialize!
}
