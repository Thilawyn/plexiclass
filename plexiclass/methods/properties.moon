{
    parse_properties: (values) =>
        for property in *@__properties_by_order
            if @[ property.name ] == nil
                v = values[ property.name ]
                if v == nil
                    v = @@[ property.name ]

                @[ property.name ] = v
}
