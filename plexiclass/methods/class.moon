export _
import error, pairs, rawget from _G

import is from require "plexiclass.type"


{
    --
    -- Metamethods
    --

    __tostring: =>
        "class: #{ @name }"


    --
    -- Type
    --

    get_name: =>
        @__name or "<Anonymous>"

    type: =>
        "class"

    is: (t) =>
        t == "class"   or
        t == @@        or
        t == @@__name

    inherits: (t) =>
        cls = @

        while cls
            if t == cls                        or
               cls.__name and t == cls.__name
                return true

            cls = cls.__parent

        false

    has_trait: (t) =>
        for trait in *@__traits_by_order
            if t == trait                      or
               trait.name and t == trait.name
                return true

        false


    --
    -- Subclasses
    --

    --- Creates a new class that inherits from the current one.
    extend: (a1, a2) =>
        if     a1 == nil          and a2 == nil           -- class\extend!
            @@create _, @
        elseif is(a1, "string")   and a2 == nil           -- class\extend name
            @@create a1, @
        elseif is(a1, "function") and a2 == nil           -- class\extend definition
            @@create _, @, a1
        elseif is(a1, "string")   and is(a2, "function")  -- class\extend name, definition
            @@create a1, @, a2
        else
            error "bad arguments to '#{ @__name }.extend'. Valid calls:
    - #{ @__name }:extend()
    - #{ @__name }:extend(string)
    - #{ @__name }:extend(function)
    - #{ @__name }:extend(string, function)"

    --- Converts a MoonScript class and its inheritance hierarchy, and have the top parent class inherit the current one.
    extend_with_moonscript_class: (moonscript_cls) =>
        parent = moonscript_cls.__parent and @extend_with_moonscript_class(moonscript_cls.__parent) or @

        parent\extend moonscript_cls.__name, =>
            for k, v in pairs moonscript_cls
                if k != "__init"    and
                   k != "__base"    and
                   k != "__name"    and
                   k != "__parent"  and
                   k != "extend"         -- Classes overriding extend can cause problems
                    cls [k]: v

            if initialize = rawget moonscript_cls, "__init"
                obj :initialize

            for k, v in pairs moonscript_cls.__base
                if k != "__index"  and
                   k != "__class"
                    obj [k]: v

    --- Initializes the class.
    initialize: =>


    --
    -- Object creation
    --

    __call: (...) =>
        @create ...

    --- Creates a class instance.
    create: (...) =>
        with @__instance_prototype\create_object!
            \initialize ...
}
