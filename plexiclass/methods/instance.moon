{
    --
    -- Metamethods
    --

    __tostring: =>
        "object: #{ @@name }"


    --
    -- Type
    --

    type: =>
        @@__name or "object"

    is: (t) =>
        t == "object"   or
        @@inherits(t)   or
        @@has_trait(t)


    --- Initializes the object.
    initialize: (values = {}) =>
        @parse_properties values
}
