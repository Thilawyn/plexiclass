import is              from require "plexiclass.type"
import pairs, tostring from _G


{
    parse: (data, structure) =>
        structure or= {}
        structure.class = @

        @parse_as data, structure

    parse_as: (data, structure) =>
        if not structure
            return data

        if is data, "table"
            for k, v in pairs data
                data[ k ] = @parse_as v, structure.keys and structure.keys[ k ] or structure.all

        if structure.transform
            data = structure.transform data
        if structure.class
            data = structure.class\from_data data

        data

    from_data: (data) =>
        with @__instance_prototype\create_object_with_values data
            \initialize!

    to_data: =>
        tostring @
}
