import copy_pairs, copy_ipairs, copy_metamethods from require "plexiclass.util"
import insert, remove from table
import is, type from require "plexiclass.type"

metamethods = require "plexiclass.methods.metamethods"
properties  = require "plexiclass.methods.properties"


class Prototype

    new: (parent) =>
        -- Inherit the values of the parent prototype
        if parent
            @mixin parent
            @__parent_prototype = parent

        -- Else, just mix the default values in
        else
            @mixin metamethods
            @mixin properties

        -- Inherit the properties of the parent, if any
        @inherit_properties parent

    clone_prototype: =>
        setmetatable copy_pairs(@, {}), getmetatable @


    --
    -- Object creation
    --

    --- Creates an object from this prototype.
    create_object: =>
        obj =
            __prototype:  @
            __values:     {}
        copy_metamethods obj.__prototype, obj

        setmetatable obj, obj

    --- Creates an object from this prototype with a specified value table.
    create_object_with_values: (values) =>
        obj =
            __prototype:  @
            __values:     values
        copy_metamethods obj.__prototype, obj

        setmetatable obj, obj


    --
    -- Mixins
    --

    --- Mixes the values of the given table into the prototype.
    mixin: (t) =>
        copy_pairs t, @


    --
    -- Properties
    --

    --- Inherit the properties of the parent prototype.
    inherit_properties: (parent) =>
        @__properties_by_order = {}

        if parent
            copy_ipairs parent.__properties_by_order, @__properties_by_order

    --- Defines a property.
    -- If a property of the same name already exists, it will be updated with the given values.
    set_property: (name, prop) =>
        -- Update existing property
        if old_prop = @property_definition name
            prop = copy_pairs prop, old_prop

        -- Create a new one
        else
            prop.name = name
            insert @__properties_by_order, prop

        -- (Re)create the setter
        @[ "set_" .. name ] = (k, v) =>
            if prop.before
                v = prop.before @, k, v

            if prop.type
                assert (prop.nilable and v == nil) or is(v, prop.type), "invalid type for property '#{ prop.name }' (#{ prop.type } expected, got #{ type v })"

            if prop.after
                v = prop.after @, k, v

            @__values[ k ] = v

    --- Removes a property.
    remove_property: (name) =>
        if i = @property_order name
            remove @__properties_by_order, i
            @[ "set_" .. name ] = nil

    --- Returns the load order of a property, or nil if it doesn't exist.
    property_order: (name) =>
        for i = 1, #@__properties_by_order
            if @__properties_by_order[i].name == name
                return i

    --- Returns the definition of a property, or nil if it doesn't exist.
    property_definition: (name) =>
        if i = @property_order name
            @__properties_by_order[i]


    --
    -- Magic attributes
    --

    --- Sets a deferred attribute.
    set_deferred_attribute: (name, fn) =>
        @[ "get_" .. name] = (...) =>
            v = fn @, ...
            @__values[ name ] = v

            v


:Prototype
