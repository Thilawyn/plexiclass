import copy_ipairs from require "plexiclass.util"
import is from require "plexiclass.type"
import insert from table

import Prototype         from require "plexiclass.prototypes.Prototype"
import InstancePrototype from require "plexiclass.prototypes.InstancePrototype"

cls_methods   = require "plexiclass.methods.class"
set           = require "plexiclass.methods.set"
cls_serialize = require "plexiclass.methods.class_serialize"


class ClassPrototype extends Prototype

    new: (name, parent_cls) =>
        -- Parent prototype
        parent = parent_cls and parent_cls.__prototype
        super parent

        -- Default values
        if not parent
            @mixin cls_methods
            @mixin set
            @mixin cls_serialize

        -- Inherit the traits of the parent, if any
        @inherit_traits parent

        @__name   = name
        @__parent = parent_cls

        -- Create the prototype for the instances of the class
        @__instance_prototype = InstancePrototype parent and parent.__instance_prototype

    clone_prototype: =>
        with super\clone_prototype!
            .__instance_prototype = .__instance_prototype\clone_prototype!


    --
    -- Traits
    --

    --- Inherit the traits of the parent prototype.
    inherit_traits: (parent) =>
        @__traits_by_order = {}

        if parent
            copy_ipairs parent.__traits_by_order, @__traits_by_order

    --- Adds a trait to the prototype.
    add_trait: (trait) =>
        if is(trait, "plexiclass.Trait") and not @has_trait trait
            insert @__traits_by_order, trait

    --- Returns whether or not the prototype contains the given trait, or the trait of the given name.
    has_trait: (t) =>
        for trait in *@__traits_by_order
            if t == trait or (trait.name and t == trait.name)
                return true

        false


:ClassPrototype
