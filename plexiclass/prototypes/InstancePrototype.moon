import Prototype from require "plexiclass.prototypes.Prototype"

instance           = require "plexiclass.methods.instance"
instance_serialize = require "plexiclass.methods.instance_serialize"


class InstancePrototype extends Prototype

    new: (parent) =>
        super parent

        -- Default values
        if not parent
            @mixin instance
            @mixin instance_serialize


:InstancePrototype
