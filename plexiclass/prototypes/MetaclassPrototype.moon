import Prototype from require "plexiclass.prototypes.Prototype"
metaclass = require "plexiclass.methods.metaclass"


class MetaclassPrototype extends Prototype

    new: (parent) =>
        super parent

        -- Default values
        if not parent
            @mixin metaclass


:MetaclassPrototype
