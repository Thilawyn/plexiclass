import
    pairs
    debug
from _G


-- The list of all the Lua metamethods that can be used overrided by your classes
-- (copied from http://lua-users.org/wiki/MetatableEvents)
metamethods = {
    "__index"
    "__newindex"
    "__mode"
    "__call"
    "__metatable"
    "__tostring"
    "__len"
    "__pairs"
    "__ipairs"
    "__gc"

    "__unm"
    "__add"
    "__sub"
    "__mul"
    "__div"
    "__idiv"
    "__mod"
    "__pow"
    "__concat"

    "__band"
    "__bor"
    "__bxor"
    "__bnot"
    "__shl"
    "__shr"

    "__eq"
    "__lt"
    "__le"
}


{
    :metamethods

    ---
    is_metamethod: do
        t = setmetatable {}, __index: -> false

        for metamethod in *metamethods
            t[ metamethod ] = true

        t

    ---
    copy_pairs: (fr, to) ->
        for k, v in pairs fr
            to[k] = v

        to

    ---
    copy_ipairs: (fr, to) ->
        for v in *fr
            to[ #to + 1 ] = v

        to

    ---
    copy_metamethods: (fr, to) ->
        for k in *metamethods
            if v = fr[k]
                to[k] = v

        to

    ---
    setfenv: setfenv or (f, env) ->
        i = 1
        while true
            name = debug.getupvalue(f, i)
            if name == "_ENV"
                debug.upvaluejoin f, i, (-> env), 1
                break
            elseif not name
                break

            i += 1

        f
}
