export _

import Collection from require "plexiclass.Collection"
cjson = require "cjson"

import is from require "plexiclass.type"

import
    error
    tostring
    setmetatable
from _G

import insert, remove, sort, concat from table

unpack or= table.unpack


--- @class plexiclass.List
List: Collection\extend "plexiclass.List", =>

    --
    -- Getters
    --

    --- Length of the list.
    obj get length: =>
        #@__values

    --- First element of the list.
    obj get first: =>
        @[ 1 ]

    --- Last element of the list.
    obj get last: =>
        @[ @length ]


    obj append: (l) =>
        l\each (_, v) ->
            @insert_last v

        @

    obj append_table: (t) =>
        for v in *t
            @insert_last v

        @


    ---
    obj __tostring: =>
        "[#{ @join ", " }]"


    obj range: (fr = 1, to = @length, f) =>
        fr = @length + fr  if fr < 0
        to = @length + to  if to < 0

        t = @__values
        for i = fr, to
            f i, t[ i ]

        @

    obj range_until: (fr = 1, to = @length, f) =>
        fr = @length + fr  if fr < 0
        to = @length + to  if to < 0

        t = @__values
        for i = fr, to
            res = { f i, t[ i ] }
            if #res > 0
                return unpack res

    ---
    obj each: (f) =>
        @range _, _, f

    obj each_until: (f) =>
        @range_until _, _, f


    obj insert: (k, v) =>
        insert @__values, k, v

        @

    obj insert_first: (v) =>
        @insert 1, v

        @

    obj insert_last: (v) =>
        @insert @length + 1, v

        @


    obj remove: (i) =>
        remove @__values, i

        @

    --- Removes the first element of the list.
    obj remove_first: =>
        @remove 1

        @

    --- Removes the last element of the list.
    obj remove_last: =>
        @remove @length

        @


    --
    -- Sorting
    --

    obj sort: (...) =>
        @clone!\sort_self ...

    obj sort_by_asc: (...) =>
        @clone!\sort_self_by_asc ...

    obj sort_by_desc: (...) =>
        @clone!\sort_self_by_desc ...

    --- Sorts the list according to the given function.
    obj sort_self: (f) =>
        sort @__values, f

        @

    obj sort_self_by_asc: (k) =>
        @sort_self (a, b) ->
            if k
                a[k] < b[k]
            else
                a < b

    obj sort_self_by_desc: (k) =>
        @sort_self (a, b) ->
            if k
                a[k] > b[k]
            else
                a > b


    --
    -- Filtering
    --

    obj accept: (f) =>
        l = @@!
        @each (k, v) ->
            if f k, v
                l\insert_last v

        l

    obj reject: (f) =>
        l = @@!
        @each (k, v) ->
            if not f k, v
                l\insert_last v

        l

    obj accept_self: (f) =>
        -- TODO

    obj reject_self: (f) =>
        -- TODO


    ---------------------------------
    -- String splitting and joining
    ---------------------------------

    cls split: (str, delimiter) =>
        l = @!
        pattern = "([^%s]+)"\format delimiter

        str\gsub pattern, (c) ->
            l\insert_last c
            nil

        l

    obj join: (delimiter) =>
        concat @map((_, v) -> tostring v).__values, delimiter


    --
    -- Serialization
    --

    obj to_data: =>
        data = parent @
        if cjson.array_mt
            setmetatable data, cjson.array_mt

        data


    obj unpack: =>
        unpack @__values
