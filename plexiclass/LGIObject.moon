import Object from require "plexiclass.Object"
import
    pcall
    type
from _G


lgi_object_value = (k) =>
    @__values[ k ]


LGIObject: Object\extend "plexiclass.LGIObject", =>

    --- Instance creation
    cls __call: (...) =>
        with @create @create_lgi_object ...
            \initialize ...

    --- Creates and returns an instance of the LGI class.
    cls create_lgi_object: (...) =>
        @.__lgi_class ...

    cls from_lgi_object: (obj) =>


    obj initialize: (...) =>
        parent.initialize @, ...

    -- obj __index: (k) =>
    --     print "LGIObject.__index: #{ k }"
    --     if getter = @__prototype.__getters_by_name[ k ]  -- If there is a getter for that index
    --         return getter @

    --     v = pcall @lgi_object_value, k
    --     v != nil        and
    --         v           or     -- If the value table contains the given index
    --         @__prototype[ k ]  -- Else, search the prototype

    obj __index: (k) =>
        ok, v = pcall lgi_object_value, @, k
        if ok and v != nil
            print k, type(v)
            if type(v) == "function"
                return (_, ...) -> v @__values, ...
            else
                return v

        @__prototype[ k ]
