export _

import Class from require "plexiclass.Class"
import is    from require "plexiclass.type"


--- A piece of code definition that can be included in a class.
Trait: Class "plexiclass.Trait", =>

    --
    -- Type
    --

    obj type: =>
        @@__name or "trait"

    obj is: (t) =>
        t == "trait"    or
        @@inherits(t)   or
        @@has_trait(t)

    obj __tostring: =>
        @name or "trait"


    cls __call: (a1, a2) =>
        if     a1 == nil          and a2 == nil           -- Trait!
            @create!

        elseif is(a1, "string")   and a2 == nil           -- Trait name
            @create name: a1

        elseif is(a1, "function") and a2 == nil           -- Trait definition
            @create definition: a1

        elseif is(a1, "string")   and is(a2, "function")  -- Trait name, definition
            @create name: a1, definition: a2

        else
            error "bad arguments to '#{ @@name }'. Valid calls:
    - #{ @@name }()
    - #{ @@name }(string)
    - #{ @@name }(function)
    - #{ @@name }(string, function)"


    obj pro name:
        type:     "string"
        nilable:  true

    cls definition: =>
    obj pro definition:
        type: "function"
