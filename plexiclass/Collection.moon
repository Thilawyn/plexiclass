import
    setmetatable
    assert
    getmetatable
from _G

import Object   from require "plexiclass.Object"
import is, type from require "plexiclass.type"


Collection: Object\extend "plexiclass.Collection", =>

    --
    -- Metamethods
    --

    obj __len: =>
        @length


    --
    -- Object creation
    --

    --- Initializes the object.
    obj initialize: (a1, ...) =>
        parent @

        if is a1, @@  -- The first argument is an instance of the same class
            @append a1

        elseif is a1, "table"  -- It's a table
            @append_table a1

        elseif a1 != nil
            @append_table {a1, ...}


    --
    -- Mode
    --

    --- Returns the mode of the collection table.
    obj get mode: =>
        if mt = getmetatable @__values
            mt.__mode

    --- Sets the mode of the collection table.
    obj set mode: (k, v) =>
        assert v == "k" or v == "v" or v == nil, "invalid value for 'mode' (\"k\", \"v\" or nil expected, got #{ v })"

        -- Get the metatable of the value table, or create it if it doesn't exist
        mt = getmetatable @__values
        if not mt
            mt = {}
            setmetatable @__values, mt

        mt.__mode = v

        @


    --
    -- Research
    --

    --- Returns whether or not the collection contains the given element.
    obj has: (el) =>
        @find(el) != nil

    --- Returns the index of the first occurence of the given element, if it exists in the collection. Otherwise nil.
    obj find: (el) =>
        @each_until (k, v) ->
            el == v and k or nil

    --- Returns the value and the key of first element of the collection that passes the given truth test. Otherwise nil.
    obj first_where: (f) =>
        @each_until (k, v) ->
            if f k, v
                return v, k

    --- Returns the value and the key of the first element of the collection with the given key/value pair. Otherwise nil.
    obj first_where_key: (key, val) =>
        @first_where (k, v) ->
            v[ key ] == val


    --
    -- Map, reduce
    --

    obj transform: (f) =>
        @each (k, v) ->
            val = f k, v
            if val != nil
                @[ k ] = val

        @

    obj map: (f) =>
        @clone!\transform f

    obj reduce: (f, acc) =>
        @each (k, v) ->
            acc = f acc, k, v

        acc


    --
    -- Operations
    --

    --- Creates a new collection that is the concatenation of the two given collections.
    obj __add: (c) =>
        assert is(c, @@), "invalid type for right operand (#{ @@ } expected, got #{ type c })"

        with @@!
            \append @
            \append c
