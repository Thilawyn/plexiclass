import type from _G


{
    ---
    type: (obj) ->
        t = type obj

        if (t == "table" or t == "userdata") and
           type(obj.type) == "function"
            obj\type!
        else
            t

    ---
    is: (obj, ty) ->
        t = type obj

        if (t == "table" or t == "userdata") and
           type(obj.is) == "function"
            obj\is ty
        else
            t == ty
}
