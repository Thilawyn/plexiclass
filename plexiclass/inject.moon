-- Luvit compatibility
if pcall require, "luvi"
    -- Preload the scripts with Luvit's loader
    with package.loaded
        ["plexiclass"] = require "./init"


for k, v in pairs require "plexiclass"
    _G[ k ] = v
