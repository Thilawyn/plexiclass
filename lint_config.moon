import inject_whitelist_globals from require "plexiclass.linter"


{
    whitelist_globals: {
        [""]: inject_whitelist_globals {}
    }
}
